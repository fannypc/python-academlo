# Programa para contar la recurrencia de palabras en un archivo con clases

# Lista que guarda el nombre de los archivos
nombres_archivos = []
# Diccionario que guarda intancias de los archivos
archivos = dict()


# Clase para procesar los archivos
class Archivo:
    nombre = ''

    def __init__(self, nombre):
        self.nombre = nombre

    def contador_palabras(self):
        palabras = dict()
        contador = 0

        try:
            obj_archivo = open(self.nombre)

            for linea in obj_archivo:
                linea = linea.split()
                for palabra in linea:
                    if palabra in palabras:
                        palabras[palabra] = palabras[palabra] + 1
                    else:
                        palabras[palabra] = 1

            palabras_ordenadas = sorted(palabras.items(), key=lambda k: k[1], reverse=True)
            palabras_ordenadas = dict(palabras_ordenadas)
            print(f"Las 10 palabras mas repetidas en el archivo [{self.nombre}] son las siguientes:")
            for llave in palabras_ordenadas:
                print(llave, '=', palabras_ordenadas[llave])
                contador += 1
                if contador == 10:
                    break
        except:
            print(f'Problema al intentar abrir el archivo: {self.nombre}')


# Si queremos utilizar de forma independiente los objetos utilizamos un diccionario, aunque
# para este ejemplo tambien podemos evitar el uso de diccionarios y utilizar el objecto al
# momento de crearlo
def recorrer_dict():
    for nombre in nombres_archivos:
        print("\n==========================")
        obj_archivo = archivos[nombre]
        print(f'Nombre del archivo: {obj_archivo.nombre}')
        obj_archivo.contador_palabras()


def recorrer_lista(nombres_archivos):
    for item in nombres_archivos:
        nuevo_archivo = Archivo(item)
        archivos[nuevo_archivo.nombre] = nuevo_archivo

    recorrer_dict()


while True:
    archivo = input("Ingresa el nombre del archivo. Escribe 'done' para terminar:\n").strip()
    if archivo == 'done':
        break
    elif not archivo:
        print("** Por favor ingresa un nombre o 'done' para terminar **")
    else:
        nombres_archivos.append(archivo)

recorrer_lista(nombres_archivos)
