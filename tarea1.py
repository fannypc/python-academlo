# Programa para contar la recurrencia de palabras en un archivo

def contador_palabras(obj_archivo):
    palabras = dict()

    for linea in obj_archivo:
        linea = linea.split()
        for palabra in linea:
            if palabra in palabras:
                palabras[palabra] = palabras[palabra] + 1
            else:
                palabras[palabra] = 1

    print("La recurrencia de las palabras en el archivo es la siguiente: \n")
    for llave in palabras:
        print(llave, ':', palabras[llave])


while True:
    try:
        archivo = input("Ingresa el nombre del archivo. Escribe 'done' para terminar:\n")
        if archivo == 'done':
            break
        else:
            obj_archivo = open(archivo)
            contador_palabras(obj_archivo)
    except:
        print("No se encontro el archivo")